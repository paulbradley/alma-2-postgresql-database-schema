

CREATE TABLE Titles (
    --↑ reference table holding all possible values for a persons title

    TitleId   SERIAL        NOT NULL PRIMARY KEY,
    Title     varchar(9)    UNIQUE NOT NULL
);


CREATE TABLE ExternalSystems (
    --↑ reference table holding a list of external systems that can be
    --  used to authenticate a given user of the system - examples would
    --  be the Windows account name and the Quadramed user id.

    ExternalSystemId  SERIAL      NOT NULL PRIMARY KEY,
    SystemName        varchar(20) UNIQUE NOT NULL
);


CREATE TABLE Employees (
    --↑ data table holding a record for each employee who can access ALMA

    EmployeeId              SERIAL        NOT NULL PRIMARY KEY,
    TitleId                 int           NOT NULL DEFAULT(1) REFERENCES Titles (TitleId),
    Forenames               varchar(35)   NOT NULL,
    Surname                 varchar(30)   NOT NULL,
    Initials                varchar(4)    ,
    InitialsOnLetter        varchar(4)    ,
    EmailAccount            varchar(100)  ,
    GMCNumber               varchar(8)    ,
    ContactDetails          varchar(1000) ,

    BaseFontSize            int           NOT NULL DEFAULT 14,
    BackgroundColour        varchar(6)    NOT NULL DEFAULT 'FFFFFF',

    DisplayFullNameOnLetter boolean       NOT NULL DEFAULT false,
    DisplayEmailOnLetter    boolean       NOT NULL DEFAULT true,
    Administrator           boolean       NOT NULL DEFAULT false,
    AccountEnabled          boolean       NOT NULL DEFAULT true,
    Consultant              boolean       NOT NULL DEFAULT false,
    DepartmentEditor        boolean       NOT NULL DEFAULT false,
    DepartmentIT            boolean       NOT NULL DEFAULT false,
    WaitingListUser         boolean       NOT NULL DEFAULT false,
    LastActivity            timestamp     NOT NULL,
    MigrationCode           varchar(10)   UNIQUE NOT NULL
);


CREATE TABLE EmployeeAutoTexts (
    --↑ data table to store an employees auto text entries

    EmployeeId        int             NOT NULL PRIMARY KEY
                                               REFERENCES Employees (EmployeeID),
    CorrectOrReplace  char(1)         NOT NULL DEFAULT('R'),
    FindText          varchar(400)    NOT NULL,
    ReplaceText       varchar(1000)   NOT NULL,

    CONSTRAINT UQ_EmployeeAutoTexts_Unique UNIQUE (EmployeeId, FindText)
);


CREATE TABLE EmployeeExternalSystemMappings (
    --↑ data table storing the account details for any external system
    --  which could be used to authenticate the user

    EmployeeId          int           NOT NULL REFERENCES Employees (EmployeeID),
    ExternalSystemId    int           NOT NULL REFERENCES ExternalSystems (ExternalSystemId),
    AccountHash         char(32)      NOT NULL,
    AccountCredential   varchar(12)   NOT NULL,

    CONSTRAINT UQ_EmployeeExternalSystemMappings_Unique UNIQUE (EmployeeId, AccountCredential)
);

CREATE INDEX IDX_EmployeeExternalSystemMappings_AccountHash
          ON EmployeeExternalSystemMappings(AccountHash ASC)
       WHERE AccountHash IS NOT NULL;


CREATE TABLE EmployeeDocumentDefaults (
    --↑ data table to hold the document defaults set by the employee
    --  these defaults will be used on all letters created after the
    --  defaults have been set until the employee clears them

    EmployeeId            int     NOT NULL PRIMARY KEY
                                           REFERENCES Employees (EmployeeID),
    CreatedOnBehalfOfId   int     NOT NULL REFERENCES Employees (EmployeeID),
    LeadConsultantId      int     NOT NULL REFERENCES Employees (EmployeeID),
    SignatoryId           int     NOT NULL REFERENCES Employees (EmployeeID),
    AdmissionDate         date,
    ClinicDate            date,
    DictatedDate          date,
    DischargedDate        date,
    SeenDate              date
);


CREATE TABLE EmployeeJobTitles (
    --↑ data table to store an employees different job titles
    --  junior doctors in particular can have more than one job title

    EmployeeId    int           NOT NULL REFERENCES Employees (EmployeeID),
    JobTitle      varchar(190)  NOT NULL,

    CONSTRAINT UQ_EmployeeJobTitles_Unique UNIQUE (EmployeeId, JobTitle)
);


CREATE TABLE EmployeeWaitingListEmployees (
    --↑ data table to store which consultants a user in the waiting list
    --  department is interested in following

    EmployeeId        int    NOT NULL REFERENCES Employees (EmployeeID),
    ConsultantId      int    NOT NULL REFERENCES Employees (EmployeeID),

    CONSTRAINT UQ_EmployeeWaitingListEmployees_Unique
        UNIQUE (EmployeeId, ConsultantId)
);


CREATE TABLE Specialties (
    --↑ data table to store the clinical specialties

    SpecialtyId     SERIAL        NOT NULL PRIMARY KEY,
    SpecialtyName   varchar(35)   NOT NULL UNIQUE
);


CREATE TABLE EmployeeSpecialties (
    --↑ data table to hold references to which specialties employees have
    --  access to and which of those is their preferred specialty

    EmployeeId        int       NOT NULL REFERENCES Employees (EmployeeID),
    SpecialtyId       int       NOT NULL REFERENCES Specialties (SpecialtyId),
    DefaultSpecialty  boolean   NOT NULL DEFAULT false
);


CREATE TABLE ClinicTitles (
    --↑ data table to the possible titles a clinic can have, some clinics
    --  have different kinds of patients registered onto the same template

    ClinicTitleId     SERIAL        NOT NULL PRIMARY KEY,
    ClinicTitle       varchar(50)   NOT NULL,
    SpecialtyId       int           NOT NULL REFERENCES Specialties (SpecialtyId)
);


CREATE TABLE Genders (
    --↑ reference table holding all possible values for a persons gender

    GenderId  SERIAL        NOT NULL PRIMARY KEY,
    Gender    varchar(7)    UNIQUE NOT NULL
);


CREATE TABLE Patients (
    --↑ data table holding all registered patients, being a complete copy
    --  of the PMI as ALMA doesn't want to rely on an external database

    MRN             bigint          NOT NULL PRIMARY KEY,
    MergedMRN       bigint          ,
    NHSNumber       varchar(10)     ,
    GenderId        smallint        NOT NULL DEFAULT(1) REFERENCES Genders (GenderId),
    TitleId         smallint        NOT NULL DEFAULT(1) REFERENCES Titles (TitleId),
    Forenames       varchar(35)     ,
    Surname         varchar(40)     ,
    Organisation    varchar(65)     ,
    AddressLine1    varchar(80)     ,
    AddressLine2    varchar(80)     ,
    AddressLine3    varchar(45)     ,
    AddressLine4    varchar(45)     ,
    Postcode        varchar(10)     ,
    GPPracticeCode  varchar(10)     ,
    DateOfBirth     date            ,
    DateOfDeath     date            ,
    APIUpdateTime   timestamp       ,

    PatientRequestedLargePrintCopies            boolean NOT NULL DEFAULT false,
    PatientDeclinedToRecieveGPCopies            boolean NOT NULL DEFAULT false,
    PatientDeclinedToRecieveGPCopiesRecordedBy  int NOT NULL REFERENCES Employees (EmployeeID),
    DatePatientDeclinedGPCopies                 date,

    --TODO
    --SchoolId
    --DentistId
);

CREATE INDEX IDX_Patients_MergedMRN
          ON Patients(MergedMRN ASC)
       WHERE MergedMRN IS NOT NULL;

CREATE INDEX IDX_Patients_NHSNumber
          ON Patients(NHSNumber ASC)
       WHERE NHSNumber IS NOT NULL;

CREATE INDEX IDX_Patients_APIUpdateTime
          ON Patients(APIUpdateTime DESC)
       WHERE APIUpdateTime IS NOT NULL;


CREATE TABLE Documents (
    --↑ data table to hold all the document meta data

    DocumentId            BIGSERIAL   NOT NULL PRIMARY KEY,
    MRN                   bigint      NOT NULL REFERENCES Patients (MRN),
    IsDeleted             boolean     NOT NULL DEFAULT false,

    CreatedDateTime       timestamp   NOT NULL,
    EventDateTime         timestamp   NOT NULL,
    UpdatedDateTime       timestamp   NOT NULL,

    CreatedById           smallint    REFERENCES Employees (EmployeeId),
    CreatedForId          smallint    REFERENCES Employees (EmployeeId),
    DepartmentId          smallint    DEFAULT(-1),
    DocumentTypeId        smallint    DEFAULT(-1),
    LastUpdatedById       smallint    REFERENCES Employees (EmployeeId),
    LockedById            smallint    REFERENCES Employees (EmployeeId),
    SignatoryId           smallint    REFERENCES Employees (EmployeeId),

    DisplayName           varchar(150)

    --TODO
    /*
      ,[AddresseeType]
      ,[AddresseeID]
      ,[qCPREmployeeID]
      ,[ClinicNamePrefixID]
      ,[qCPRProcedureID]
      ,[ClinicDate]
      ,[AdmissionDate]
      ,[DischargedDate]
      ,[StatusCode]
      ,[CommentCategoryID]
      ,[Publishable]
      ,[DictatedNotSigned]
      ,[OverideParentGuardianMessage]
      ,[PrivateConfidentialAddressTag]
      ,[ccAddressSingleLine]
      ,[ccPatient]
      ,[ccFile]
      ,[PublishedDateTime]
      ,[PublishedByNT1000]
      ,[PDFCreatedDateTime]
      ,[DocumentInsertTag]
      ,[WardName]
      ,[SeenDate]
      ,[DictatedDate]
      ,[UseDearForename]
      ,[DocumentTitleDate]
      ,[SentToEvolve]
      ,[SpecialtyName]
      ,[AssignedConsultantInitials]
      ,[datVisit_VisitNumber]
      ,[ConsultantID]
      ,[ConsultantInitials]
    */
);


CREATE INDEX IDX_Documents_MRN
          ON Documents(MRN ASC);

CREATE INDEX IDX_Documents_EventDateTime
          ON Documents(EventDateTime DESC);

CREATE INDEX IDX_Documents_All
          ON Documents(MRN, DepartmentId, IsDeleted, EventDateTime);


CREATE TABLE Z_Documents (
    --↑ data table to hold nightly updates to sync the version 2
    --  database with newly published documents from version 1

    DocumentId            bigint,
    MRN                   bigint,
    IsDeleted             boolean,

    CreatedDateTime       timestamp,
    EventDateTime         timestamp,
    UpdatedDateTime       timestamp,

    CreatedById           int,
    CreatedForId          int,
    DepartmentId          int,
    DocumentTypeId        int,
    LastUpdatedById       int,
    LockedById            int,
    SignatoryId           int,

    DisplayName           varchar(150)
);


CREATE TABLE DocumentPostscriptNotes (
    --↑ ??
    --

    DocumentId        bigint        NOT NULL PRIMARY KEY,
    PostscriptNote    text          NOT NULL
);
