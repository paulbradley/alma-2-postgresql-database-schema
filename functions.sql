
CREATE OR REPLACE FUNCTION initDocumentsTable()
    RETURNS integer as $$
BEGIN
  FOR I IN 1 .. 1000000 LOOP
    INSERT INTO Documents (MRN, CreatedDateTime, EventDateTime, UpdatedDateTime)
      VALUES (9999999, '1990-01-01 00:00:00', '1990-01-01 00:00:00', '1990-01-01 00:00:00');
  END LOOP;
RETURN 1;
END;
$$ LANGUAGE plpgsql;
