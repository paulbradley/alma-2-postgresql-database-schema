DROP TABLE IF EXISTS Titles                         CASCADE;
DROP TABLE IF EXISTS ExternalSystems                CASCADE;
DROP TABLE IF EXISTS Employees                      CASCADE;
DROP TABLE IF EXISTS EmployeeAutoTexts              CASCADE;
DROP TABLE IF EXISTS EmployeeDocumentDefaults       CASCADE;
DROP TABLE IF EXISTS EmployeeJobTitles              CASCADE;
DROP TABLE IF EXISTS EmployeeSpecialties            CASCADE;
DROP TABLE IF EXISTS EmployeeWaitingListEmployees   CASCADE;
DROP TABLE IF EXISTS EmployeeExternalSystemMappings CASCADE;
DROP TABLE IF EXISTS Specialties                    CASCADE;
DROP TABLE IF EXISTS ClinicTitles                   CASCADE;
DROP TABLE IF EXISTS Documents                      CASCADE;
DROP TABLE IF EXISTS DocumentPostscriptNotes        CASCADE;
DROP TABLE IF EXISTS Genders                        CASCADE;
DROP TABLE IF EXISTS Patients                       CASCADE;

DROP TABLE IF EXISTS Z_Documents                    CASCADE;
