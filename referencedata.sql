
-- EXTERNAL SYSTEMS
INSERT INTO ExternalSystems (SystemName) VALUES ('Windows NT Account');
INSERT INTO ExternalSystems (SystemName) VALUES ('Quadramed ID');

-- TITLES
INSERT INTO Titles (Title) VALUES ('Unknown');
INSERT INTO Titles (Title) VALUES ('Mr');
INSERT INTO Titles (Title) VALUES ('Mrs');
INSERT INTO Titles (Title) VALUES ('Miss');
INSERT INTO Titles (Title) VALUES ('Master');
INSERT INTO Titles (Title) VALUES ('Ms');
INSERT INTO Titles (Title) VALUES ('Professor');
INSERT INTO Titles (Title) VALUES ('Rabbi');
INSERT INTO Titles (Title) VALUES ('Reverend');
INSERT INTO Titles (Title) VALUES ('Sir');
INSERT INTO Titles (Title) VALUES ('Sister');
INSERT INTO Titles (Title) VALUES ('Lord');
INSERT INTO Titles (Title) VALUES ('Lady');
INSERT INTO Titles (Title) VALUES ('Honorable');
INSERT INTO Titles (Title) VALUES ('Father');
INSERT INTO Titles (Title) VALUES ('Dr');
INSERT INTO Titles (Title) VALUES ('Dame');
INSERT INTO Titles (Title) VALUES ('Brother');

-- GENDERS
INSERT INTO Genders (Gender) VALUES ('Unknown');
INSERT INTO Genders (Gender) VALUES ('Female');
INSERT INTO Genders (Gender) VALUES ('Male');


-- EMPLOYEES
INSERT INTO Employees (TitleId, Forenames, Surname, Initials, EmailAccount, MigrationCode, LastActivity)
VALUES (2, 'Paul', 'Bradley', 'PB', 'paul.bradley@lthtr.nhs.uk', 'PAULX030', now());


INSERT INTO EmployeeExternalSystemMappings (EmployeeId, ExternalSystemId, AccountHash, AccountCredential)
VALUES (1, 1, 'XOXO', 'PAULX030');

INSERT INTO EmployeeExternalSystemMappings (EmployeeId, ExternalSystemId, AccountHash, AccountCredential)
VALUES (1, 2, 'XOXO', '357');

